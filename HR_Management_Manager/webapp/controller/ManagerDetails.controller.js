sap.ui.define([
	//'sap/base/util/deepExtend',
	//'sap/m/ColumnListItem',
	//'sap/m/Input',
	"./Formatter",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/Text",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/library",
	"sap/ui/export/library",
	"sap/ui/export/Spreadsheet"
], function (Formatter, Controller, JSONModel, MessageToast, Text, Button, Dialog, mobileLibrary, library, Spreadsheet) {
	"use strict";

	return Controller.extend("HR_Management_Manager.HR_Management_Manager.controller.ManagerDetails", {
		onInit: function () {
			this.odataFunctionRead();
		},
		odataFunctionRead: function () {
			this.getView().setBusy(true);
			var that = this;
			var serviceUrl = "/HR_Management_ManagerHR_Management_Manager/dbHR/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);
			var reqStatus = "I";

			//Filter
			var filterRows = [];
			//Apply filters to ANAGRAFICA_DIP
			var managerInsertedRows = new sap.ui.model.Filter("STATUS", sap.ui.model.FilterOperator.EQ, reqStatus);
			filterRows.push(managerInsertedRows);

			that.getInsertedRequests(that, serviceUrl, modelColl);
			that.getOnlyEmployeesFilter(that, serviceUrl, modelColl);
		},
		doFilter: function (oEvent) {
			this.getView().setBusy(true);
			var that = this;
			var serviceUrl = "/HR_Management_ManagerHR_Management_Manager/dbHR/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);
			var reqStatus = "I";
			var idUtente = that.getView().byId("idSomeComboBox").getSelectedKey();
			//Filter
			var filterRows = [];
			//Apply filters to FERIE_DIP
			var managerInsertedRows = new sap.ui.model.Filter("STATUS", sap.ui.model.FilterOperator.EQ, reqStatus);
			filterRows.push(managerInsertedRows);
			if (idUtente !== "") {
				var userRows = new sap.ui.model.Filter("UTENTE_ID", sap.ui.model.FilterOperator.EQ, idUtente);
				filterRows.push(userRows);
			}

			//Read entity resp
			modelColl.read("/FERIE_DIP", {
				filters: [filterRows],
				async: true,
				success: function (oResult) {
					that.getView().setBusy(false);
					var results = oResult.results;
					var oModel = new JSONModel({
						FERIE_DIP: results
					});
					that.getView().setModel(oModel, "ManagerModel");
				},
				error: function (oError) {
					that.getView().setBusy(false);
					var msg = "Attenzione: errore nella lettura dati";
					MessageToast.show(msg);
				}
			});
		},
		getInsertedRequests: function (that, serviceUrl, modelColl) {
			var reqStatus = "I";

			//Filter
			var filterRows = [];
			//Apply filters to FERIE_DIP
			var managerInsertedRows = new sap.ui.model.Filter("STATUS", sap.ui.model.FilterOperator.EQ, reqStatus);
			filterRows.push(managerInsertedRows);

			//Read entity resp
			modelColl.read("/FERIE_DIP", {
				filters: [filterRows],
				async: true,
				success: function (oResult) {
					that.getView().setBusy(false);
					var results = oResult.results;
					var oModel = new JSONModel({
						FERIE_DIP: results
					});
					that.getView().setModel(oModel, "ManagerModel");
				},
				error: function (oError) {
					that.getView().setBusy(false);
					var msg = "Attenzione: errore nella lettura dati";
					MessageToast.show(msg);
				}
			});
		},
		onApprove: function (oEvent) {
			var that = this;
			//console.log(oEvent);
			var oTable = that.getView().byId("respTable");
			var aContexts = oTable.getSelectedContexts();
			var today = new Date();
			//var noteResp = that.getView().byId("NoteResp").getValue();

			if (aContexts.length === 0) {
				var message = "Attenzione! Selezionare almeno un record";
				MessageToast.show(message);
			} else {
				var oDialog = new Dialog({
					title: "Confirm",
					type: "Message",
					content: new Text({
						text: "Approvare lo stato dei record selezionati?"
					}),
					beginButton: new Button({
						type: mobileLibrary.ButtonType.Emphasized,
						text: "Conferma",
						press: function () {
							var serviceUrl = "/HR_Management_ManagerHR_Management_Manager/dbHR/odata/v2/CatalogService/";
							var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

							for (var i = aContexts.length - 1; i >= 0; i--) {
								var idRichiesta = aContexts[i].getObject().ID_RICHIESTA;

								var entityFerieUpdate = {
									STATUS: "A",
									DATA_MODIFICA_STATUS: today
										//NOTE_RESPONSABILE: noteResp.toString()
								};

								modelColl.update("/FERIE_DIP(" + idRichiesta + ")", entityFerieUpdate, {
									success: function (data) {
										that.odataFunctionRead();
										var messageModified = "Stato ferie approvato";
										MessageToast.show(messageModified);
									},
									error: function (e) {
										var messageError = "Errore imprevisto nella modifica dello stato ferie";
										MessageToast.show(messageError);
									}
								});
							}
							oDialog.close();
						}
					}),
					endButton: new Button({
						text: "Cancel",
						press: function () {
							oDialog.close();
							var messageAnnulla = "Operazione annullata";
							MessageToast.show(messageAnnulla);
						}
					}),
					afterClose: function () {
						oDialog.destroy();
					}
				});
				oDialog.open();
			}
		},
		onReject: function (oEvent) {
			var that = this;
			//console.log(oEvent);
			var oTable = that.getView().byId("respTable");
			var aContexts = oTable.getSelectedContexts();
			var today = new Date();
			//var noteResp = that.getView().byId("NoteDip").getValue();

			if (aContexts.length === 0) {
				var message = "Attenzione! Selezionare almeno un record";
				MessageToast.show(message);
			} else {
				var serviceUrl = "/HR_Management_ManagerHR_Management_Manager/dbHR/odata/v2/CatalogService/";
				var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

				var oDialog = new Dialog({
					title: "Confirm",
					type: "Message",
					content: new Text({
						text: "Rifiutare lo stato dei record selezionati?"
					}),
					beginButton: new Button({
						type: mobileLibrary.ButtonType.Emphasized,
						text: "Conferma",
						press: function () {

							for (var i = aContexts.length - 1; i >= 0; i--) {
								var idRichiesta = aContexts[i].getObject().ID_RICHIESTA;

								var entityFerieUpdate = {
									STATUS: "R",
									DATA_MODIFICA_STATUS: today
										//NOTE_RESPONSABILE: noteResp.toString()
								};

								modelColl.update("/FERIE_DIP(" + idRichiesta + ")", entityFerieUpdate, {
									success: function (data) {
										that.odataFunctionRead();
										var messageModified = "Stato ferie rifiutato";
										MessageToast.show(messageModified);
									},
									error: function (e) {
										var messageError = "Errore imprevisto nella modifica dello stato ferie";
										MessageToast.show(messageError);
									}
								});
							}
							oDialog.close();
						}
					}),
					endButton: new Button({
						text: "Cancel",
						press: function () {
							oDialog.close();
							var messageAnnulla = "Operazione annullata";
							MessageToast.show(messageAnnulla);
						}
					}),
					afterClose: function () {
						oDialog.destroy();
					}
				});
				oDialog.open();
			}
		},
		onExport: function () {
			var EdmType = library.EdmType;

			var oSpreadsheet = new Spreadsheet({
				dataSource: this.getView().getModel("ManagerModel").getProperty("/FERIE_DIP"),
				fileName: "Ferie dipendente.xlsx",
				workbook: {
					columns: [{
						label: "Id richiesta",
						property: "ID_RICHIESTA",
						type: EdmType.Number
					}, {
						label: "Data inizio",
						property: "DATA_INIZIO",
						type: EdmType.Date,
						width: 15
					}, {
						label: "Data fine",
						property: "DATA_FINE",
						type: EdmType.Date,
						width: 15
					}, {
						label: "Giorni di ferie",
						property: "GG_FERIE",
						type: EdmType.Number,
						width: 5
					}, {
						label: "Data richiesta",
						property: "DATA_RICHIESTA",
						type: EdmType.Date,
						width: 15
					}, {
						label: "Stato richiesta",
						property: "STATUS",
						type: EdmType.String,
						width: 5
					}, {
						label: "Id responsabile",
						property: "ID_RESPONSABILE",
						type: EdmType.String
					}, {
						label: "Data stato",
						property: "DATA_MODIFICA_STATUS",
						type: EdmType.Date,
						width: 15
					}, {
						label: "Note dipendente",
						property: "NOTE_DIPENDENTE",
						type: EdmType.String
					}, {
						label: "Note responsabile",
						property: "NOTE_RESPONSABILE",
						type: EdmType.String
					}, {
						label: "Id utente",
						property: "UTENTE_ID",
						type: EdmType.String
					}]
				}
			});

			oSpreadsheet.build().finally(function () {
				oSpreadsheet.destroy();
			});

		},
		getOnlyEmployeesFilter: function (that, serviceUrl, modelColl) {
			var employeeStat = "D";

			//Filter
			var filters = [];
			//Apply filters to ANAGRAFICA_DIP
			var onlyEmployee = new sap.ui.model.Filter("RUOLO", sap.ui.model.FilterOperator.EQ, employeeStat);
			filters.push(onlyEmployee);

			//Read entity Employees
			modelColl.read("/ANAGRAFICA_DIP", {
				filters: [filters],
				async: true,
				success: function (oResult) {
					var results = oResult.results;

					var jsonModel = new JSONModel({
						ANAGRAFICA_DIP: results
					});
					that.getView().setModel(jsonModel, "Employees");

					that.getView().setBusy(false);
				},
				error: function (oError) {
					that.getView().setBusy(false);
					var msg = "Attenzione errore";
					MessageToast.show(msg);
				}
			});
		},

		//formatter button
		buttonHighlightFormatter: function (parameter) {
			var value;
			switch (parameter) {
			case "I":
				value = "Information";
				break;
			case "A":
				value = "Success";
				break;
			case "R":
				value = "Error";
				break;
			default:
				value = "None";
			}
			return value;
		}
	});
});