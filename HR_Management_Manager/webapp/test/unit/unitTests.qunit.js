/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"HR_Management_Manager/HR_Management_Manager/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});