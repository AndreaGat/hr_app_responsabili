/*global QUnit*/

sap.ui.define([
	"HR_Management_Manager/HR_Management_Manager/controller/ManagerDetails.controller"
], function (Controller) {
	"use strict";

	QUnit.module("ManagerDetails Controller");

	QUnit.test("I should test the ManagerDetails controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});